<?php
$installer = $this;

$installer->getConnection()->addColumn(
                $installer->getTable('clickmap/links'),
                'url',
                array(
                    'type'      => 'text',
                    'comment'   => 'Url'
                )
            );

$installer->endSetup();