<?php
$installer = $this;

$installer->run("
    ALTER TABLE `{$installer->getTable('clickmap/links')}`
        ADD `status` int(1) NOT NULL  DEFAULT '0',
        ADD `modified` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        ADD `refreshed` TIMESTAMP;
");

$installer->endSetup();