<?php

$installer = $this;
$installer->startSetup();

$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('clickmap/points')};
    DROP TABLE IF EXISTS {$this->getTable('clickmap/links')};

    CREATE TABLE `{$installer->getTable('clickmap/points')}` (
      `point_id` int(11) NOT NULL auto_increment,
      `x_point` text,
      `y_point` text,
      `link_id` text,
      PRIMARY KEY  (`point_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->run("
    CREATE TABLE `{$installer->getTable('clickmap/links')}` (
      `link_id` int(11) NOT NULL auto_increment,
      `link` text,
      PRIMARY KEY  (`link_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");    

$installer->endSetup();