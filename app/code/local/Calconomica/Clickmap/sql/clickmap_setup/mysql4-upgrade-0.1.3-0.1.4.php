<?php
$installer = $this;

$installer->run("
    ALTER TABLE `{$installer->getTable('clickmap/links')}`
        ADD `intens` int(10) NOT NULL  DEFAULT '0',
        ADD `max_points` int(10) NOT NULL  DEFAULT '0',
        ADD `clicks` int(10) NOT NULL  DEFAULT '0' ;
");

$installer->endSetup();