<?php
$installer = $this;

$installer->run("
    ALTER TABLE `{$installer->getTable('clickmap/points')}` 
        ADD `time` 
        TIMESTAMP 
        DEFAULT CURRENT_TIMESTAMP ;
");

$installer->endSetup();