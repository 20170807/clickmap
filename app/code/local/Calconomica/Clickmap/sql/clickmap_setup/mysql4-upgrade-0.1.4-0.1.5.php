<?php
$installer = $this;

$installer->setConfigData('clickmap_options/clickmapgroup/clickmap', 1);
$installer->setConfigData('clickmap_options/clickmapgroup/raduis', 10);
$installer->setConfigData('clickmap_options/clickmapgroup/theme', 0);
$installer->setConfigData('clickmap_options/clickmapgroup/gamma', 0);
$installer->setConfigData('clickmap_options/clickmapgroup/secure', 0);
$installer->setConfigData('clickmap_options/clickmapgroup/debug', 0);

$installer->endSetup();