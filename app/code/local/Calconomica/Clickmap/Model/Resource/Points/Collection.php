<?php

class Calconomica_Clickmap_Model_Resource_Points_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct() {
            $this->_init('clickmap/points');
    }

    public function getSize() {
        if (is_null($this->_totalRecords)) {
            $sql = $this->getSelectCountSql();
            $this->_totalRecords = $this->getConnection()->fetchOne($sql, $this->_bindParams);
        }
        return intval($this->_totalRecords);
    }     
}