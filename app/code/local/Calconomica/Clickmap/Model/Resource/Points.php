<?php

class Calconomica_Clickmap_Model_Resource_Points extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct() {
        $this->_init('clickmap/points', 'point_id');
    }
}