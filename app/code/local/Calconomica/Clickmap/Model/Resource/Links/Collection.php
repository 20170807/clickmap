<?php

class Calconomica_Clickmap_Model_Resource_Links_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct() {
            $this->_init('clickmap/links');
    }
}