<?php
class Calconomica_Clickmap_Model_Source_Gamma
{
    public function toOptionArray() {
        return array(
            array('value'=>0, 'label'=>'Red - Yellow'),
            array('value'=>1, 'label'=>'Red - Blue'),
            array('value'=>2, 'label'=>'Blue - Yellow'),
        );
    }
}