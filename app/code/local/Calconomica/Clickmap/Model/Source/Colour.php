<?php
class Calconomica_Clickmap_Model_Source_Radius
{
    public function toOptionArray() {
        return array(
            array('value'=>3, 'label'=>3),
            array('value'=>5, 'label'=>5),
            array('value'=>7, 'label'=>7),
            array('value'=>10, 'label'=>10),
            array('value'=>12, 'label'=>12),            
        );
    }
}