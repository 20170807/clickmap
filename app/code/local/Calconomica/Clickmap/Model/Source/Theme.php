<?php
class Calconomica_Clickmap_Model_Source_Theme
{
    public function toOptionArray() {
        return array(
            array('value'=>0, 'label'=>'None'),
            array('value'=>1, 'label'=>'Dark')
        );
    }
}