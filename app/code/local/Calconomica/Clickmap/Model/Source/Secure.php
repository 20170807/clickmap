<?php
class Calconomica_Clickmap_Model_Source_Secure
{
    public function toOptionArray() {
        return array(
            array('value'=>0, 'label'=>'http://'),
            array('value'=>1, 'label'=>'https://')
        );
    }
}