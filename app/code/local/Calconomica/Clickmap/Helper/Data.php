<?php
class Calconomica_Clickmap_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getInsertUrl() {
        return $this->_getUrl('clickmap/points/insert');
    }

    public function getSelectUrl() {
        return $this->_getUrl('clickmap/points/select');
    }
}