<?php

class Calconomica_Clickmap_PointsController extends Mage_Core_Controller_Front_Action {

    /*
     * insert Points
     */
    public function insertAction () 
    {
        $response = array();
        $params = $this->getRequest()->getParams();
        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $debug = Mage::getStoreConfig('clickmap_options/clickmapgroup/debug');

        $xCoords    = explode(',', $params['xCoords']);
        $yCoords    = explode(',', $params['yCoords']);
        $urlIds     = explode(',', $params['urlIds']);

        $saved = 0;
        foreach ($urlIds as $i => $urlId) {
            if (!$urlId || !is_numeric($urlId))
                continue;

            $response['savePoint'][] = $this->savePoint($xCoords[$i], $yCoords[$i], $urlIds[$i]);

            $saved++;
        }

        $link = Mage::getModel('clickmap/links')->load($urlIds[$i]);

        $clicks = $link->getData('clicks');
        $clicks += $saved;

        $link->setClicks($clicks)->save();
        $response['saveClicks'] = sprintf('Was saved %s clicks. Total: %s', $saved, $clicks);

        if($debug)
            $this->getResponse()->setBody(json_encode($response));
    }

    /*
     * save Points
     */
    protected function savePoint($x, $y, $linkId) {
        $return = array();
        $count = Mage::getModel('clickmap/points')
                ->getCollection()
                ->addFieldToFilter('link_id', $linkId)
                ->getSize();

        $maxPoints = Mage::getModel('clickmap/links')
                ->load($linkId)
                ->getData('max_points');

        if(empty($maxPoints)){
            $return[] = 'Field clickmap_links.max_points is empty. First Item is not deleting.';
        } else {
            if ($count > $maxPoints) {
                Mage::getModel('clickmap/points')
                    ->getCollection()
                    ->addFieldToFilter('link_id', $linkId)
                    ->getFirstItem()
                    ->delete();

                $return[] = 'First Click was deleted';
            }
        }

        $pointsModel = Mage::getModel('clickmap/points')->setData(array(
            'x_point' => $x, 
            'y_point' => $y, 
            'link_id' => $linkId
        ))->save();

        $return[] = sprintf('Click %s was saved', $pointsModel->getId());
        $return[] = sprintf('Data Base has %s clicks for link %s ', $count, $linkId);

        return $return;
    }

    /*
     * select Points
     */
    public function selectAction () {
        $params = $this->getRequest()->getParams();
        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);

        $pointsModelCollection = Mage::getModel('clickmap/points')
                ->getCollection()
                ->addFieldToSelect(array('x_point', 'y_point'))
                ->addFieldToFilter('link_id', $params['urlId']);

        $count = $pointsModelCollection->getSize();

        $intens = Mage::getModel('clickmap/links')
                ->load($params['urlId'])
                ->getData('intens');

        if(empty($intens))
        {
            $this->getResponse()->setBody(json_encode(array(
                'error' => true,
                'message' => 'Intens is empty.'
            )));

            return $this;
        }

        $intensity = ceil($count / $intens);

        if ($intensity > 1)
            $pointsModelCollection->getSelect()->where('point_id % ? = 0', $intensity);

        $points = $pointsModelCollection->getData();

        $this->getResponse()->setBody(json_encode($points));
    }
}