<?php

class Calconomica_Clickmap_AdminlinksController extends Mage_Adminhtml_Controller_Action {

    /*
     * view Click Map List
     */
    public function indexAction() {
        $this->loadLayout();
        $block = $this->getLayout()->createBlock('clickmap/links');
        $this->_addContent($block);
        $this->renderLayout();
    }

    /*
     * view Link form for update
     */
    public function editAction() {
        $id = $this->getRequest()->getParam('id', null);

        $model = Mage::getModel('clickmap/links');

        if ($id) {
            $model->load((int) $id);

            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError('Link does not exist');
                $this->_redirect('*/*/');
            }
        }

        Mage::register('links_data', $model);

        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $block = $this->getLayout()->createBlock('clickmap/links_edit');
        $this->_addContent($block);
        $this->renderLayout();
    }

    /*
     * view new Link form
     */
    public function newAction() {
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $block = $this->getLayout()->createBlock('clickmap/links_new');
        $this->_addContent($block);
        $this->renderLayout();
    }

    /*
     * delete Link
     */
    public function deleteAction() {
        $linkId = $this->getRequest()->getParam('id', null);


        if (!$linkId) {
            $this->_redirect('*/*/');
        } else {
            if (!$this->_deleteLinkWithPoints($linkId))
                $this->_redirect('*/*/');

            Mage::getSingleton('adminhtml/session')->addSuccess('Link was deleted');
            $this->_redirect('*/*/');
        }
    }

    protected function _deleteLinkWithPoints($linkId)
    {
        $link = Mage::getModel('clickmap/links');
        $link->load((int) $linkId);

        if (!$link->getId()) {
            Mage::getSingleton('adminhtml/session')->addError('Link does not exist');
            return false;
        }

        $link->delete();

        $pointsModelCollection = Mage::getModel('clickmap/points')->getCollection();
        $pointsModelCollection->addFieldToFilter('link_id', $linkId);

        foreach ($pointsModelCollection as $pointModel){
             $pointModel->delete();
        }

        return true;
    }

    /*
     * update Link
     */
    public function saveAction() {
        if (!$data = $this->getRequest()->getPost()) {
            Mage::getSingleton('adminhtml/session')->addError('No data found to save');
            $this->_redirect('*/*/');

            return;
        }

        $model = Mage::getModel('clickmap/links');
        $id = $this->getRequest()->getParam('id');

        if ($id)
            $model->load($id);

        $baseUrl = Mage::getBaseUrl();
        $parsedBaseUrl = parse_url($baseUrl);

        $url = str_replace(array('http://', 'https://'), '', $data['url']);

        if (strpos($url, $parsedBaseUrl['host']) === false) {
            Mage::getSingleton('adminhtml/session')->addError('Please, enter correct data. Url should be from current host.');

            if ($model->getId()) {
                $this->_redirect('*/*/edit', array('id' => $model->getId()));
            } else {
                $this->_redirect('*/*/');
            }

            return;
        }

        $urlNoBase = str_replace($parsedBaseUrl['host'], '', $url);
        $urlNoIndex = str_replace('index.php', '', $urlNoBase);

        $link = trim($urlNoIndex, '/');

        if ($link == '')
            $link = '/';

        $currentTimestamp = Mage::getModel('core/date')->timestamp(time());

        $newLink = array(
            'link' => $link, 
            'url' => $url,
            'status' => $data['status'],
            'intens' => $data['intens'],
            'max_points' => $data['max_points'],
            'modified' => $currentTimestamp
        );

        if (!$id)
            $newLink['refreshed'] = $currentTimestamp;

        $model->addData($newLink);
        $model->save();

        Mage::getSingleton('adminhtml/session')->addSuccess('Link was successfully saved.');

        if ($this->getRequest()->getParam('back')) {
            $this->_redirect('*/*/edit', array('id' => $model->getId()));
        } else {
            $this->_redirect('*/*/');
        }
    }

    /*
     * refresh Points for Link
     */
    function refreshAction() {
        if (!$linkId = $this->getRequest()->getParam('id')) {
            Mage::getSingleton('adminhtml/session')->addError('There is not Link Id in the requeste');
            $this->_redirect('*/*/');
            return;
        }

        $pointsModelCollection = Mage::getModel('clickmap/points')->getCollection();
        $pointsModelCollection->addFieldToFilter('link_id', $linkId);

        foreach ($pointsModelCollection as $pointModel){
             $pointModel->delete();
        }

        $currentTimestamp = Mage::getModel('core/date')->timestamp(time());
        $model = Mage::getModel('clickmap/links');
        $model->load($linkId);
        $model->setData('refreshed', $currentTimestamp);
        $model->setData('clicks', 0);
        $model->save();

        Mage::getSingleton('adminhtml/session')->addSuccess('Clicks were deleted.');

        $this->_redirect('*/*/');
    }

    /*
     * mass deleting Links
     */
    function massDeleteAction() {
        $links = $this->getRequest()->getParam('link_id');

        foreach($links as $link){
            if (!$this->_deleteLinkWithPoints($link))
                $this->_redirect('*/*/');
        }

        Mage::getSingleton('adminhtml/session')->addSuccess('Links were deleted.');
        $this->_redirect('*/*/');
    }

    /*
     * mass refreshing Points for Links
     */
    function massRefreshAction() {
        $links = $this->getRequest()->getParam('link_id');
        
        
        foreach($links as $link){
            $pointsModelCollection = Mage::getModel('clickmap/points')->getCollection();
            $pointsModelCollection->addFieldToFilter('link_id', $link);

            foreach ($pointsModelCollection as $pointModel){
                $pointModel->delete();
            }

            $currentTimestamp = Mage::getModel('core/date')->timestamp(time());
            $model = Mage::getModel('clickmap/links');
            $model->load($link);
            $model->setData('refreshed', $currentTimestamp);
            $model->setData('clicks', 0);
            $model->save();
        }

        Mage::getSingleton('adminhtml/session')->addSuccess('Links were refreshed.');

        $this->_redirect('*/*/');
    }

    /*
     * mass disabling Links
     */
    function massDisableAction() {
        $links = $this->getRequest()->getParam('link_id');

        foreach($links as $link){
            $currentTimestamp = Mage::getModel('core/date')->timestamp(time());
            $model = Mage::getModel('clickmap/links');
            $model->load($link);
            $model->setData('status', 0);
            $model->setData('modified', $currentTimestamp);
            $model->save();
        }

        Mage::getSingleton('adminhtml/session')->addSuccess('Links were disabled.');

        $this->_redirect('*/*/');
    }

    /*
     * mass enabling Links
     */
    function massEnableAction() {
        $links = $this->getRequest()->getParam('link_id');

        foreach($links as $link){
            $currentTimestamp = Mage::getModel('core/date')->timestamp(time());
            $model = Mage::getModel('clickmap/links');
            $model->load($link);
            $model->setData('status', 1);
            $model->setData('modified', $currentTimestamp);
            $model->save();
        }

        Mage::getSingleton('adminhtml/session')->addSuccess('Links were activated.');

        $this->_redirect('*/*/');
    }
}