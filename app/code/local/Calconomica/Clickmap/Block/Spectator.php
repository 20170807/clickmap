<?php
class Calconomica_Clickmap_Block_Spectator extends Mage_Core_Block_Template
{    
    public function getCurrentLinkModel() {
        $currentUrl = Mage::helper('core/url')->getCurrentUrl();
        $url = Mage::getSingleton('core/url')->parseUrl($currentUrl);

        $path = $url->getPath();
        $path = str_replace('index.php', '', $path);
        $path = trim($path, '/');

        if ($path == '') {
            $path = '/';
        }

        $linkModel = Mage::getModel('clickmap/links')->load($path, 'link');

        return $linkModel;
    }
}