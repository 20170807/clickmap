<?php

class Calconomica_Clickmap_Block_Links extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct() {
        $this->_blockGroup = 'clickmap';
        $this->_controller = 'links';
        $this->_headerText = $this->__('Click Map');
 
        parent::__construct();
    }

    protected function _prepareLayout() {
        $this->setChild( 'grid',
            $this->getLayout()->createBlock( $this->_blockGroup.'/' . $this->_controller . '_grid',
            $this->_controller . '.grid')->setSaveParametersInSession(true) );
        return parent::_prepareLayout();
    }
}