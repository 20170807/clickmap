<?php
class Calconomica_Clickmap_Block_Links_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();

        $this->setDefaultSort('id');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('clickmap/links')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('link_id');
        $this->getMassactionBlock()->setFormFieldName('link_id');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> $this->__('Delete'),
            'url'  => $this->getUrl('*/*/massDelete'),
            'confirm' => $this->__('Are you sure?')
        ));

        $this->getMassactionBlock()->addItem('refresh', array(
            'label'=> $this->__('Refresh'),
            'url'  => $this->getUrl('*/*/massRefresh'),
            'confirm' => $this->__('Are you sure?')
        ));

        $this->getMassactionBlock()->addItem('disable', array(
            'label'=> $this->__('Disable'),
            'url'  => $this->getUrl('*/*/massDisable'),
            'confirm' => $this->__('Are you sure?')
        ));
        
        $this->getMassactionBlock()->addItem('enable', array(
            'label'=> $this->__('Enable'),
            'url'  => $this->getUrl('*/*/massEnable'),
            'confirm' => $this->__('Are you sure?')
        ));
    }

    protected function _prepareColumns()
    {
        $this->addColumn('link_id', array(
            'header'    => 'ID',
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'link_id',
        ));

        $this->addColumn('link', array(
            'header'    => 'Link',
            'align'     => 'left',
            'index'     => 'link',
        ));

        $this->addColumn('url', array(
            'header'    => 'Url',
            'align'     => 'left',
            'index'     => 'url',
        ));

        $this->addColumn('clicks', array(
                'header'    => 'Clicks',
                'index'     => 'clicks',
                'width'     => '70px',
                'sortable'  => false,
                'filter'    => false,
        ));
        
        $this->addColumn('modified', array(
                'header'    => 'Modified',
                'width'     => '100px',
                'type' => 'datetime',
                'index'     => 'modified'
        ));   

        $this->addColumn('refreshed', array(
                'header'    => 'Refreshed',
                'width'     => '100px',
                'type' => 'datetime',
                'index'     => 'refreshed'
        ));   

        $this->addColumn('status', array(
                'header'    => 'Status',
                'type'  => 'options',
                'width'     => '70px',
                'index'     => 'status',
                'options' => array(0 => 'Disabled', 1 => 'Enabled')
        ));

        $this->addColumn('clickmap', array(
                'header'    => 'To Clickmap',
                'width'     => '100px',
                'sortable'  => false,
                'filter'    => false,
                'renderer'  => 'clickmap/links_grid_renderer_clickmap',                
                
        ));

        $this->addColumn('tolink', array(
                'header'    => 'To Page',
                'width'     => '70px',
                'sortable'  => false,
                'filter'    => false,
                'renderer'  => 'clickmap/links_grid_renderer_tolink',                
                
        ));

        $this->addColumn('action', array(
                'header'    => 'Action',
                'type'      => 'action',
                'getter'     => 'getId',
                'width'     => '50px',
                'actions'   => array(
                    array(
                        'caption' => 'View',
                        'url'     => array(
                            'base'=>'*/*/edit',
                        ),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}