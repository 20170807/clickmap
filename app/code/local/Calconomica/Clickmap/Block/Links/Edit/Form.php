<?php
class Calconomica_Clickmap_Block_Links_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm() {
        if (Mage::registry('links_data')) {
            $data = Mage::registry('links_data')->getData();
        } else {
            $data = array();
        }

        if (!$linkId = $this->getRequest()->getParam('id')){
            Mage::getSingleton('adminhtml/session')->addSuccess('There is not Link Id in the request');
            return $this;
        }

        $link = Mage::getModel('clickmap/links')->load($linkId);

        $form = new Varien_Data_Form(array(
                'id'        => 'edit_form',
                'action'    => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
                'method'    => 'post',
                'enctype'   => 'multipart/form-data',
        ));
 
        $form->setUseContainer(true);

        $this->setForm($form);

        $fieldset = $form->addFieldset('clickmap_form', array(
             'legend' => $this->__('Link Information')
        ));

        $fieldset->addField('link_id', 'text', array(
             'label'        => 'ID',
             'disabled'     => 'disabled',
             'required'     => false,
             'name'         => 'link_id',
        ));

        $fieldset->addField('url', 'text', array(
             'label'     => $this->__('Url'),
             'class'     => 'required-entry',
             'required'  => true,
             'name'      => 'url',
        ));        

        $fieldset->addField('link', 'text', array(
             'label'        => $this->__('Link'),
             'disabled'     => 'disabled',
             'name'         => 'link',
        ));

        $fieldset->addField('clicks', 'text', array(
             'label'        => $this->__('Total Clicks'),
             'disabled'     => 'disabled',
             'name'         => 'clicks',
        )); 

        $ssl = Mage::getStoreConfig('clickmap_options/clickmapgroup/secure') ? "https://" : "http://";

        $fieldset->addField('clickmap', 'note', array(
            'name'  => 'clickmap',
            'text'  => '<a target=\'_blank\' href="' . $ssl . $link->getData('url') . '/?clickmap=true" >To Click Map >></a>'
        ));

        $fieldset->addField('tolink', 'note', array(
            'name'  => 'clickmap',
            'text'  => '<a target=\'_blank\' href=\'' . $ssl . $link->getData('url') . '\' >To Page >></a>'
        ));
        
        $fieldsetOptions = $form->addFieldset('clickmap_options', array(
             'legend' => $this->__('Link Options')
        ));

        $fieldsetOptions->addField('status', 'select', array(
            'label'     => $this->__('Status'),
            'name'      => 'status',
            'options' => array(0 => 'Disabled', 1 => 'Enabled')
        ));

        $fieldsetOptions->addField('intens', 'select', array(
            'label'     => $this->__('Max Intensity'),
            'name'      => 'intens',
            'options' => array(
                3000 => '3000',
                6000 => '6000',
                8000 => '8000',
                11000 => '11000'
            ),
            'note' => 'Max Intensity. When number of points more than it intensity of drawing will be less.'
        ));

        $fieldsetOptions->addField('max_points', 'select', array(
            'label'     => $this->__('Data Base Limit'),
            'name'      => 'max_points',
            'options' => array(
                5000 => '5000',
                7000 => '7000',
                10000 => '10000',
                20000 => '20000'
            ),
            'note' => 'Max count clicks in database.'
        ));

        $pointsModelCollection = Mage::getModel('clickmap/points')->getCollection();
        $pointsModelCollection->addFieldToFilter('link_id', $linkId);

        $count = $pointsModelCollection->getSize();

        $limit = $link->getData('max_points');

        $return = !empty($count) ? ($count / $limit) : '0';
        $return = $return * 100;

        $fieldsetOptions->addField('filling', 'note', array(
            'label' => $this->__('Data Base Limit is reached on'),
            'name'  => 'filling',
            'text'  => $return . ' %'
        ));

        $fieldsetAction = $form->addFieldset('clickmap_action', array(
             'legend' =>'Action'
        ));

        $resetLink = $this->getUrl('*/*/refresh', array('id' => $this->getRequest()->getParam('id')));
        $fieldsetAction->addField('refresh', 'note', array(
            'label' => $this->__('Delete all tracking data from this Click Map'),
            'name'  => 'button',
            'text'  => $this->getLayout()
                ->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => $this->__('Refresh'),
                    'onclick'   => 'deleteConfirm(\'' . $this->__('Are you sure you want to refresh this Click Map?') . '\', \'' . $resetLink . '\')',
                ))
                ->toHtml(),
        ));    

        $form->setValues($data);

        return parent::_prepareForm();
    }
}