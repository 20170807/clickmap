<?php

class Calconomica_Clickmap_Block_Links_Grid_Renderer_Clickcount
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row) {
        $linkId = $row->getData('link_id');
        $pointsModelCollection = Mage::getModel('clickmap/points')->getCollection();
        $pointsModelCollection->addFieldToFilter('link_id', $linkId);

        $count = $pointsModelCollection->getSize();    

        $link = Mage::getModel('clickmap/links')->load($linkId);
        
        $limit = $link->getData('max_points');
        
        $return = !empty($count) ? ($count / $limit) : '0';
        $return = $return * 100;
        
        return $return . '%';
    }
}