<?php

class Calconomica_Clickmap_Block_Links_Grid_Renderer_Clickmap
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row) {
        $ssl = Mage::getStoreConfig('clickmap_options/clickmapgroup/secure') ? 'https://' : 'http://';
        $href = $row->getData('url');
        $href = trim($href, '/');
        $href .= '/?clickmap=true';
        return '<a href="'. $ssl . $href . '" target="_blank">'.'To Clickmap'.'</a>';
    }
}