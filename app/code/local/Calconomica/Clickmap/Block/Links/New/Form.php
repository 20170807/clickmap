<?php

class Calconomica_Clickmap_Block_Links_New_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm() {
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save'),
            'method' => 'post',
            'enctype' => 'multipart/form-data',
        ));

        $form->setUseContainer(true);

        $this->setForm($form);

        $fieldset = $form->addFieldset('clickmap_form', array(
            'legend' => $this->__('New Clickmap Link Information')
        ));

        $fieldset->addField('status', 'select', array(
            'label' => 'Status',
            'name' => 'status',
            'options' => array(0 => 'Disabled', 1 => 'Enabled'),
            'value' => 1
        ));

        $fieldset->addField('url', 'text', array(
            'label'     => 'Url',
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'url',
            'note' => 'This url will be tracked.'
        ));

        $fieldset->addField('intens', 'select', array(
           'label'     => 'Max Intensity',
           'name'      => 'intens',
           'options' => array(
                3000 => '3000',
                6000 => '6000',
                8000 => '8000',
                11000 => '11000'
           ),
           'value' => 6000,
           'note' => 'Max Intensity. When number of points more than it intensity of drawing will be less.'
        ));

        $fieldset->addField('max_points', 'select', array(
           'label'     => 'Max Points',
           'name'      => 'max_points',
           'options' => array(
                5000 => '5000',
                7000 => '7000',
                10000 => '10000',
                20000 => '20000'
           ),
           'value' => 10000,
           'note' => 'Max count clicks in database.'
        ));

        return parent::_prepareForm();
    }
}