var Drawer = Class.create();

Drawer.prototype = {
    initialize: function() {
        this.initCoords();
    },
            
    initCoords: function() {
        this.coords = [];
        this.radius = drawerRadius;
        this.theme = drawerTheme;
        this.gamma = drawerGamma;
    },

    getPoints: function(self) {
        new Ajax.Request(urlSelect, {
            requestHeaders: {Accept: 'application/json'},
            method: 'Post',
            parameters: {'urlId': urlId,
                        'lastPeriod': lastPeriod},
            onSuccess: function(response) {
                self.coords = response.responseText.evalJSON(true);
                self.createClickMap();
            }
        });
    },

    initPoint: function() {
        var c       = document.createElement('canvas');

        var ctx     = c.getContext('2d');
        var grd    = ctx.createRadialGradient(this.radius, this.radius, 0, this.radius, this.radius, this.radius);

        c.width     = 2 * this.radius;
        c.height    = 2 * this.radius;

        grd.addColorStop(0, 'rgba(0,0,0,1)');
        grd.addColorStop(1, 'rgba(0,0,0,0)');

        ctx.fillStyle = grd;
        ctx.fillRect(0, 0, 2 * this.radius, 2 * this.radius);

        return c;
    },

    createGradient: function() {
        var c      = document.createElement('canvas');
        var ctx     = c.getContext('2d');
        var grd    = ctx.createLinearGradient(0, 0, 0, 256);

        c.width    = 1;
        c.height   = 256;

        //  red-yellow
        if (this.gamma == 0) {
            var gradientOption = {
                0.1: 'rgba(128, 255, 0, 0.7)',
                0.2: 'rgba(255, 255, 0, 0.8)',
                0.7: 'rgba(234, 72, 58, 0.9)',
                1.0: 'rgba(162, 36, 25, 1)'
            };
        }

        //  red-blue
        if (this.gamma == 1) {
            var gradientOption = {
                0.1: 'rgba(0, 24, 255, 0.7)',
                0.2: 'rgba(0, 13, 139, 0.8)',
                0.7: 'rgba(234, 72, 58, 0.9)',
                1.0: 'rgba(162, 36, 25, 1)'
            };
        }

        //  blew-yellow
        if (this.gamma == 2) {
            var gradientOption = {
                0.1: 'rgba(128, 255, 0, 0.7)',
                0.2: 'rgba(255, 255, 0, 0.8)',
                0.7: 'rgba(0, 13, 139, 0.9)',
                1.0: 'rgba(0, 24, 255, 1)'
            };
        }

        for (var i in gradientOption) {
            if (gradientOption.hasOwnProperty(i)) {
                grd.addColorStop(i, gradientOption[i]);
            }
        }

        ctx.fillStyle = grd;
        ctx.fillRect(0, 0, 1, 256);

        return ctx.getImageData(0, 0, 1, 256).data;
    },

    createClickMap: function() {
        var p   = this.initPoint();
        var c  = document.getElementById('spCan');
        var ctx = c.getContext('2d');

        c.width    = window.screen.availWidth;
        c.height   = window.screen.availHeight + 100;

        setCanvasToMid();

        ctx.clearRect(0, 0, c.width, c.height);

        for (var i = 0, length = this.coords.length; i < length; i++) {
            ctx.globalAlpha = 0.3;
            ctx.drawImage(
                p,
                (this.coords[i]['x_point'] - this.radius + (c.width / 2)),
                this.coords[i]['y_point'] - this.radius
            );
        }

        var clickMapImage = ctx.getImageData(0, 0, c.width, c.height);
        this.colorize(clickMapImage.data);
        ctx.putImageData(clickMapImage, 0, 0);
    },

    colorize: function(pixels) {
        var gradient    = this.createGradient();
        var opacity     = 1;

        for (var i = 3, length = pixels.length, j; i < length; i += 4) {
            if (pixels[i]) {
                j = 4 * pixels[i];
                pixels[i - 3] = gradient[j];
                pixels[i - 2] = gradient[j + 1];
                pixels[i - 1] = gradient[j + 2];

                pixels[i] = opacity * (gradient[j + 3] || 255);
            }
        }
    }
};

var drawer = new Drawer();
drawer.getPoints(drawer);

window.onload = window.onresize = function() {
    setCanvasToMid();
};

function setCanvasToMid() {
    var canvas          = document.getElementById("spCan");
    var viewportLeft    = (document.documentElement.clientWidth - canvas.width) / 2;
    canvas.style.left   = viewportLeft + 'px';
}

window.onload = function() {
    var canvas = document.getElementById("spCan");
    if (drawerTheme) {
        canvas.style.backgroundColor = 'rgba(0, 0, 0, 0.5)';
    }
};
