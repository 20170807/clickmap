var Spectator = Class.create();

Spectator.prototype = {
    initialize: function(minWidth) {
        this.minWidth = minWidth;
        this.sendPoints();
        this.iTest = 0;
        this.counter = 0;
    },

    initCoords: function() { 
        this.counter = 0;
        Mage.Cookies.set('xCoords', '');
        Mage.Cookies.set('yCoords', '');
        Mage.Cookies.set('urlIds', '');
    },

    setCoord: function(x, y) {
        if(document.body.scrollWidth < this.minWidth)
            return;

        var xCoords = '';
        xCoords = Mage.Cookies.get('xCoords');
        Mage.Cookies.set('xCoords', xCoords + ',' + (x - Math.round(document.body.scrollWidth / 2)));
        
        var yCoords = '';
        yCoords = Mage.Cookies.get('yCoords');
        Mage.Cookies.set('yCoords', yCoords + ',' + y);
        
        var urlIds = '';
        urlIds = Mage.Cookies.get('urlIds');
        Mage.Cookies.set('urlIds', urlIds + ',' + urlId);
        
        this.counter++;
    },

    sendPoints: function() {
        if (!Mage.Cookies.get('xCoords')) {
            return;
        }

        var coords = {};
        coords['xCoords']   = Mage.Cookies.get('xCoords');
        coords['yCoords']   = Mage.Cookies.get('yCoords');
        coords['urlIds']     = Mage.Cookies.get('urlIds');
        console.log('sendPoints:' + this.counter);

        this.initCoords();
        
        
        new Ajax.Request(urlInsert, {
            method: 'Post',
            parameters: coords,
            onComplete: function(transport) {



            }
        });        
    },

    /*
     * spectator.maxTest = 100;
     * spectator.clickTest(spectator);
     */
    clickTest: function(self){
      setTimeout(function() {
        console.log(self.iTest);

        var x = Math.floor((Math.random() * 1000) + 1);
        var y = Math.floor((Math.random() * 1000) + 1);

        self.setCoord(x, y);

        if(self.iTest > self.maxTest)
        {
            self.iTest = 0;
            return;
        }

        self.clickTest(self);

        self.iTest++;
      }, 100);
    }
};

var Repeater = Class.create();

Repeater.prototype = {
    initialize: function(interval) {
        this.interval = interval;
        this.repeat(this);
    },

  forRepeating: function() {
    spectator.sendPoints();
  },

    repeat: function(self) {
        this.forRepeating();
        setTimeout(function() {self.repeat(self);}, this.interval * 1000);
    }
};


document.observe( 'click', function( event ){
    var clientY = 0;
    
    if (document.body.scrollTop) {
        clientY = document.body.scrollTop;
    }
    else {
       clientY = document.documentElement.scrollTop;
    }
    
    spectator.setCoord(event.clientX, clientY + event.clientY);
});


var spectator   = new Spectator(1024);
var repeater    = new Repeater(5);

